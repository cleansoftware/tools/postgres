# Postgres

|     Variable      | Valor por defecto |                       Links                        |
|:-----------------:|:-----------------:|:--------------------------------------------------:|
|   POSTGRES_TAG    |        13         | [Tags](https://hub.docker.com/_/postgres?tab=tags) |
|   POSTGRES_USER   |     postgres      |                         -                          |
| POSTGRES_PASSWORD | mysecretpassword  |                         -                          |


## POSTGRES_TAG

Version del postgres, en el momento de esta documentación se probó con exito la version 13 a diferencia que el traefik
por su naturaleza las versiones de postgres suelen ser mas estables y no suelen tener tantos problemas al subir versiones
no obstante no hay garantias

- [versiones](https://hub.docker.com/_/traefik?tab=tags)
- [documentacion](https://doc.traefik.io/traefik/)


## POSTGRES_USER
Usuario por defecto que se usara en el postgres para hacer login


## POSTGRES_PASSWORD

Password por defecto que se usara en el postgres para hacer login
